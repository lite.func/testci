# FROM alpine:latest

# WORKDIR /media_recv

# COPY bin/media_recv.bin bin/media_recv.bin

# CMD ["./bin/media_recv.bin", "config/config.json"]

FROM docker:git
# RUN apk add curl
# RUN curl -O https://storage.googleapis.com/golang/go1.11.linux-amd64.tar.gz
# RUN tar -C /usr/local -xzf go1.11.linux-amd64.tar.gz
# RUN export PATH=$PATH:/usr/local/go/bin

WORKDIR /go/src/cloud

RUN apk add --no-cache make musl-dev go

# Configure Go
ENV GOROOT /usr/lib/go
ENV GOPATH /go
ENV PATH /go/bin:$PATH

RUN mkdir -p ${GOPATH}/src ${GOPATH}/bin

# Install Glide
RUN go get -u github.com/Masterminds/glide/...

# WORKDIR $GOPATH

CMD ["go", "env"]

# ENTRYPOINT ./app